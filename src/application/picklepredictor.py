import os
import src.settings.base as stg
from os.path import join
from pickle import load

def predict_riskTolerance(X_input):

    model = stg.MODEL
    loaded_model = load(open(join(stg.MODELS_DIR, model), 'rb'))
    
    # estimate accuracy on validation set
    predictions = loaded_model.predict(X_input)
    return predictions