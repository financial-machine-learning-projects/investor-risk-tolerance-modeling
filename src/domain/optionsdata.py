import os
from os import path
from os.path import join
import sys

import pandas as pd
import numpy as np

import src.settings.base as stg
from src.infrastructure.datasetcreation import DataFrameBuilder
from src.domain.marketdata import assets_data

def options_list():
    
    options = np.array(assets_data().columns)
    # str(options)
    options = []

    for tic in assets_data().columns:
        #{'label': 'user sees', 'value': 'script sees'}
        mydict = {}
        mydict['label'] = tic #Apple Co. AAPL
        mydict['value'] = tic
        options.append(mydict)
        
    return options