"""Basic settings of the project.
Contains all configurations for the projectself.
Should NOT contain any secrets.
>>> import settings
>>> settings.DATA_DIR
"""
import os
from os import path
from os.path import join
import sys

# Paths
REPO_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "../../"))
DATA_DIR = os.path.join(REPO_DIR, "data")
MODELS_DIR = os.path.join(REPO_DIR, "models")


# Utils
Average_SP500_2007 = 1478
Average_SP500_2009 = 948
validation_size = 0.2
seed = 3
num_folds = 10
scoring = 'r2'

# DataFrame
TRUE_RISK_TOLERANCE = 'TrueRiskTolerance'

AGE = 'AGE07'
EDUCATION = 'EDCL07'
STATUS = 'MARRIED07'
NB_KIDS = 'KIDS07'
OCCUPATION = 'OCCAT107'
INCOME = 'INCOME07'
RISK_WILLINGNESS = 'RISK07'
NETWORTH = 'NETWORTH07'

COLS = [AGE, EDUCATION, STATUS, 
        NB_KIDS, OCCUPATION, INCOME, 
        RISK_WILLINGNESS, NETWORTH]

# Data
SCF_data = 'SCFP2009panel.xlsx'
SP500_data = 'SP500Data.csv'

# Models
MODEL = 'finalized_model.sav'
INPUT_data = 'InputData.csv'
